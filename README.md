At AQS Air our technicians are specially trained and certified to maintain, diagnose, and correct problems throughout your entire system. On-going training insures customers that our technicians service, repair and adjust all system functions to the manufacturers required specifications.

Address : 1000 N State St, Unit 119, Hemet, CA 92543

Phone : 951-487-6700